package arrays.exercises;

public class ArraysApp {
	
	public static void main(String[] args) {
		Object [][] students = new Object[3][3];
		students[0][0] = "Jonas";
		students[0][1] = "Jonaitis";
		students[0][2] = 6;
		students[1][0] = "Petras";
		students[1][1] = "Petraitis";
		students[1][2] = 2;
		students[2][0] = "Onutė";
		students[2][1] = "Jonaitienė";
		students[2][2] = 7;
		
		for (int i = 0; i < students.length; i++) {
			for (int j = 0; j < students[i].length; j++) {
				System.out.print(students[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
}
