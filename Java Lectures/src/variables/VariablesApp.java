package variables;

/**
 * Kintamųjų pavyzdžiai
 * 
 * @author Zilvinas
 *
 */
public class VariablesApp {

	
	/**
	 * Paleidžiamasis metodas
	 * 
	 * @param parametrai Konsolės parametras
	 */
	public static void main(String[] parametrai) {
		
		//Spausdins visus parametrus į konsolę
		System.out.println("### Console parameters ###");
		for (String parametras : parametrai) {
			System.out.println(parametras);
		}
		

		//Primityvių kintamųjų deklaravimas ir konvertavimas su vaidmenimis
		System.out.println("### Integer and double variables ###");
		int x, y = 9;
		double d = 3.5;
		x = (int) d;
		System.out.println(x);
		
		//Automatinis dalybos konvertavimas į float tipą
		System.out.println("### Float variables ###");
		float f = 2.7F;
		f = y/x;
		System.out.println(f);

		//Konsolės parametro nuskaitymas ir konvertavimas į int tipą
		System.out.println("### Converting console argument to int ###");
		String gradesCelsiusString = parametrai[0];
		int gradesCelsius = Integer.valueOf(gradesCelsiusString);
		System.out.println(gradesCelsius + " grades Celsius!");
	}

}
