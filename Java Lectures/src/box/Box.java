package box;

public class Box {
	
	private double width;
	
	private double height;
	
	private double depth;
	
	public Box(int w, int h, int d) {
		width = w;
		height = h;
		depth = d;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public double getDepth() {
		return depth;
	}
	
	public double getVolume(){
		return width * height * depth;
	}
	
	@Override
	public String toString() {
		return "Volume is " + getVolume();
	}
	
}
