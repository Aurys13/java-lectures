package console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * 
 * 
 * @author Zilvinas
 *
 */
public class ConsoleApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Object that handles user input
		Scanner scanner = new Scanner(System.in);
		// Variables to be added
		String a, b;
		int result;
		
		// Printing greeting and request to enter first operand
		System.out.println("### Calculator ###");
		System.out.print("Operand A: ");
		// Reading first operand
		a = scanner.nextLine();
		
		// Request to enter first operand
		System.out.print("Operand B: ");
		// Reading first operand
		b = scanner.nextLine();
		
		
		try {
			// Converting operands to integer values and calculating sum value
			result = Integer.valueOf(a) + Integer.valueOf(b);
			
			// Printing result in case of input success
			System.out.println(a + " + " + b + " = " + result);

		} catch (NumberFormatException e) {
			// In case of wrong input let user know about it
			System.out.println("At least one of the variables provided was not a number!");
		}
		System.out.println("\nThank you!\nShutting down ...");
		
		
		//Alternative to Scanner 
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
		try {
			String line = bufferReader.readLine();
			System.out.println("�ved�te tekst�: " + line);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		try {
			int number = readInt();
			System.out.println(number);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Releasing scanner resources
		scanner.close();

	}
	
	public static int readInt() throws IOException{
		int value = 0;
		char [] ch;
		int r = 0;
		while(r != 13 && r != 10 || value == 0){
			r = System.in.read();
			ch = Character.toChars(r);
			if(r != 13 && r != 10){
				if(Character.isDigit(ch[0])){
					value = (value*10) + Character.getNumericValue(ch[0]);
				}
			}
		}
		return value;
	}

}
